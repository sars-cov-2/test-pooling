const express = require("express");
const app = express();
const port = process.env.PORT || 80;

/* TODO Add proper hit counter, maybe with geographical statistics */

let hitCount = 0;

app.all("/", function (req, res, next) {
  hitCount++;
  res.header("Hit-Count", hitCount.toString());
  next();
});

app.use(express.static(__dirname + "/public", {
  extensions: ["html"]
}));

app.listen(port, () => console.log("Listening on port " + port + "."));