init();

function init() {
  let urlParams = new URLSearchParams(window.location.search);
  
  if (urlParams.has("population")) {
    $("#population").val(parseInt(urlParams.get("population")));
  }
  
  if (urlParams.has("percent")) {
    $("#percent").val(parseFloat(urlParams.get("percent")));
  }
  
  if (urlParams.has("pool")) {
    $("#pool").val(parseInt(urlParams.get("pool")));
  }
  
  $("#simulate").on("click", function () {
    runSimulation();
    history.pushState("", "", currentUrl());
  });
  $("#simulate-average").on("click", runAverageSimulation);
  $("#url").click(function () {
    $(this).select();
  });
  runAverageSimulation();
}

function runSimulation() {
  let population = parseInt($("#population").val());
  let percent = parseFloat($("#percent").val());
  let poolSize = parseInt($("#pool").val());
  
  let logText = "Simulation start. Parameters:";
  logText += "\npopulation: " + population;
  logText += "\npercent: " + percent;
  logText += "\npool size: " + poolSize;
  
  let basePositives = Math.floor(population * percent / 100);
  let positivesDifference = (population * percent / 100) - basePositives;
  let positives = basePositives + (Math.random() < positivesDifference);
  let negatives = population - positives;
  
  
  logText += "\n\nIntermediary results:";
  logText += "\naverage positive cases: " + positives;
  logText += "\napproximate positive cases this run: " + positives;
  logText += "\napproximate negative cases this run: " + negatives;
  
  
  // Build an array with the negative population
  let populationArray = new Array(negatives).fill(false);
  
  // Add the positives randomly in the negative population
  for (let i = 0; i < positives; i++) {
    let index = Math.floor(Math.random() * populationArray.length);
    populationArray.splice(index, 0, true);
  }
  
  // Build array to hold the pool chunks
  let pools = [];
  
  // Split the array in pool chunks
  for (let i = 0; i < populationArray.length; i += poolSize) {
    pools.push(populationArray.slice(i, i + poolSize));
  }
  
  // Count the chunks that have positives
  let positivePools = 0;
  for (let i = 0; i < pools.length; i++) {
    positivePools += pools[i].includes(true);
  }
  
  // Compute total tests to be done
  let secondaryTests = positivePools * poolSize;
  let totalTests = pools.length + secondaryTests;
  
  logText += "\n\nFinal results:";
  logText += "\npools with positive cases: " + positivePools;
  logText += "\ntests done for pools initially: " + pools.length;
  logText += "\ntests done for samples from pools found positive: " + secondaryTests;
  logText += "\ntotal tests done: " + totalTests + " (instead of doing " + population + " tests, one for each person)\n";
  logText += "\nURL for this simulation: <a href=\"" + currentUrl() + "\" target=\"_blank\">" + currentUrl() + "</a>";
  
  log(logText, true);
  
  $("#url").val(currentUrl());
  
  return totalTests;
}

function encodeParams() {
  let params = {
    population: parseInt($("#population").val()),
    percent: parseFloat($("#percent").val()),
    pool: parseInt($("#pool").val())
  }
  return $.param(params);
}

function currentUrl() {
  return document.location.protocol + "//" + document.location.hostname + document.location.pathname + "?" + encodeParams();
}

function runAverageSimulation() {
  log("", true);
  let testNumberTotal = 0;
  let runs = 100;
  for (let i = 0; i < runs; i++) {
    testNumberTotal += runSimulation();
  }
  let testNumberAverage = testNumberTotal / runs;
  log("\n<b>Average number of tests done using pooling: " + testNumberAverage + "</b>");
  history.pushState("", "", currentUrl());
}

function log(logText, clear) {
  // console.log(logText);
  $("#console").html((clear) ? logText : $("#console").html() + logText);
}